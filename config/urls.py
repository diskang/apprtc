from handlers.index import IndexHandler
from handlers.message import MessageHandler
from handlers.emotion import FaceEmotionHandler,VoiceEmotionHandler
from handlers.auth import LoginHandler,LogoutHandler,WelcomeHandler
url_handler_dict=[
  ('/', IndexHandler),
  ('/login', LoginHandler),
  ('/logout', LogoutHandler),
  ('/welcome',WelcomeHandler),
  ('/message', MessageHandler),
  ('/emotion/face', FaceEmotionHandler),
  ('/emotion/vocie', VoiceEmotionHandler)
]
