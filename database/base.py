#!/usr/bin/env python
# coding: utf-8
import pymssql
import config.settings as CFG

class BaseModel(object):
	conn = pymssql.connect(server=CFG.DB_SERVER, user=CFG.DB_USER, password=CFG.DB_PASSWORD, database=CFG.DB_NAME)
	cursor = conn.cursor()
	def __init__(self):
		if self.conn and self.cursor:
			pass
		else:
			print 'database not prepared'
	def _execute(self, query, params = None):
		self.cursor.execute(query, params)
	def _execute_many(self):
		pass

	def _insert(self, table, paramstring):
		sql = 'INSERT INTO %s VALUES %s'%(table,paramstring)
		# print sql
		self.cursor.execute(sql)
		self.conn.commit()

	def _get(self, table, query):
		'''select * from table where item=query
		@query: xxx=xxx
		'''
		sql = 'SELECT * FROM %s WHERE %s'%(table,query)
		self.cursor.execute(sql)
		return self.cursor

	def _delete(self):
		pass

	def _update(self, table):
		# self.cursor.execute('')
		pass