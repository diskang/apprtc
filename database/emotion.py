from base import *

class FaceStorage(BaseModel):
	"""docstring for FaceStorage"""
	def __init__(self):
		super(FaceStorage, self).__init__()
		pass

	def insert_expression(self,elder_id,datetime,value,modality='',source=''):
		table = "T_EMOTION_LOG(elder_id,emotion_log_time,emotion_modality,emotion_source,emotion_value)"
		params = "(%s,'%s','%s','%s','%s')"%(elder_id,datetime,modality,source,value)
		self._insert(table,params)

	def get_expression(self,elder_id,startTime = "",endTime="",source="",modality=""):
		table = "T_EMOTION_LOG"
		query = "elder_id=%d"%elder_id
		if source:
			query+=" AND emotion_source=%s"%source
		if modality:
			query += " AND emotion_modality=%s"%modality
		self._get(table,query)
		pass

class VoiceStorage(object):
	"""docstring for VoiceStorage"""
	def __init__(self, arg):
		super(VoiceStorage, self).__init__()
		self.arg = arg
		
