import tornado.web
from database.auth import Auth
class BaseHandler(tornado.web.RequestHandler):
    def get_current_user(self):
        return self.get_secure_cookie("username")

class LoginHandler(BaseHandler):
    def get(self):
        self.render('login.html')

    def post(self):
    	username = self.get_argument("username",None)
    	password = self.get_argument("password",None) or 'test'
    	if not (username and password):
    		self.send(400)
    	else:
    		if Auth().family_authenticate(username,password):

    			self.set_secure_cookie("username", username)
        		self.redirect(self.get_argument('next', '/welcome'))
        	else:
        		self.send(401)

class LogoutHandler(BaseHandler):
    def get(self):
    # if (self.get_argument("logout", None)):
        self.clear_cookie("username")
        self.redirect("/login")

class WelcomeHandler(BaseHandler):
    @tornado.web.authenticated
    def get(self):
        self.render('welcome.html', user=self.current_user)