import tornado.web
import datetime
from database.emotion import FaceStorage,VoiceStorage

class FaceEmotionHandler(tornado.web.RequestHandler):
	def get(self):
		pass
	def post(self):
		elder_id = self.get_argument("user_id","")
		source = self.get_argument("source","")
		modality = self.get_argument("modality","")
		logtime = self.get_argument("time","")
		emovalue = self.get_argument("emotion","")
		# logtime = str(datetime.datetime.now())
		if not (elder_id and emovalue):
			print emovalue
			self.send(400)
			return
		db = FaceStorage()
		db.insert_expression(elder_id,logtime,emovalue,modality,source)
		self.finish()

class VoiceEmotionHandler(tornado.web.RequestHandler):
	def get(self):
		db = VoiceStorage()
		self.finish()
		
	def post(self):
		pass